# Ad Tags/Units

## Installation

### Header

```html
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
  	// Define each AdUnit on the page
  	// to_v1_state_leaderboard    || Ad Code from below
  	// [ 728.0 ,  90.0 ]          || defines size of ads to show
  	// div-gpt-ad-1528560873717-0 || DIV id to place ad in
    googletag
    	.defineSlot('\/281604363\/to_v1_state_leaderboard', [ 728.0 ,  90.0 ], 'div-gpt-ad-1528560873717-0')
    	.addService(googletag.pubads())
    	.setTargeting("gender", "male"); //Example of setting slot level targeting/Key-Value Pairs

    // Causes all ad requests to be made in 1 request instead of seperate ones
    googletag.pubads().enableSingleRequest();

    // Makes sure we collapse the ad divs if we have no ad to show
    googletag.pubads().collapseEmptyDivs();

    //Example of setting page level targeting/Key-Value Pairs
    googletag.pubads().setTargeting("state","CO");

    googletag.enableServices();
  });
</script>
```

### Within Body

```html
<!-- DIV where the ad will display -->
<div id='div-gpt-ad-1528560873717-0' style='height:90px; width:728px;'>
	<script>
		googletag.cmd.push(function() { 
			googletag.display('div-gpt-ad-1528560873717-0'); 
		});
	</script>
</div>
```

**More Reading**

* [More examples](https://support.google.com/dfp_premium/answer/1638622) and documentation for ad tags
* [Key-Value Pairs](keyvalue.md) to use for targeting

## Pages Sets

Each collection is for each of the different page types on the site. 
Ad tags can be reused multipule times on a page. You just need to define 
multipule divs with different IDs and a AdSlot associated with that div. 
If defining the ad multipule times. Please ad a custom targeting to seperate 
each one from the other. 

### All Pages

Ad Code         | Name         | Size(s)      | Notes
----------------|--------------|--------------|---------------------------------------------
to_v1_all_popup | V1 All Popup | 300x250, 1x1 | PopUp Ad Unit. See [Docs](https://support.google.com/dfp_premium/answer/6088046?hl=en) for info on how to deploy

### State Pages

Ad Code                 | Name                       | Size(s)  | Notes
------------------------|----------------------------|----------|---------------------------------------------
to_v1_state_leaderboard | V1 State Leaderboard Other | 728x90   | Basic Ad Unit
to_v1_state_leader_top  | V1 State Leaderboard Top   | 728x90   | Basic Ad Unit that auto refreshes every 60s

### Trail Pages

Ad Code                      | Name                           | Size(s)          | Notes
-----------------------------|--------------------------------|------------------|----------------------------------
to_v1_trail_header_right     | V1 Trail Header Right          | 200x200, 300x250 | Basic and Video Ad for placement to the right of trail header, above map
to_v1_trail_top_half_leader  | V1 Trails Top Half Leaderboard | 728x90, 970x90   | Basic Ad Unit for use above the Waypoints
to_v1_trail_waypoints_leader | V1 Trail Waypoints Leaderboard | 728x90, 970x90   | Basic Ad Unit for use within waypoints block. Besure to populate 'waypoint' key-value/targeting for this one.