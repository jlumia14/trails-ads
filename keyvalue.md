# Available Key-Value Pairs

Key        | Possible Values
-----------|----------------
city       | Free-Form: Nearest City to trail
county     | Free-Form: County the trail is within.
state      | Free-Form: State the trail is within. Should be 2 digital code
trailname  | Free-Form: Trail Name
trailid    | Free-Form: UUID or ID of Trail being displayed
usertype   | One-Of: other, reg, unreg, writer. Reg=Registered User. UnReg=Guest/Unregistered user. Writer=Registered Contributer/Writer. Other=Anything other usecase
duplicate  | Free-Form: Unique counter/ID to seperate each duplicate instance of an ad on the page. Ideally should count up from top-bottom of page.
waypoint   | Free-Form: ID/Count of waypoint ad is displaying next to
writerid   | Free-Form: UUID or ID of trail writer/contributor
writername | Free-Form: Name of trail writer/contributor